{
  config,
  pkgs,
  lib,
  nix-xilinx,
  sops-nix,
  zremap,
  ...
}: {
  imports = [];

  system.stateVersion = "23.05";
  system.autoUpgrade.enable = false;

  sops.age.sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key"];
  sops.secrets."peerix/private" = {
    sopsFile = ./secrets/peerix.yaml;
    mode = "0400";
    owner = config.users.users.nobody.name;
    group = config.users.users.nobody.group;
  };

  nix = {
    optimise.automatic = true;
    gc.automatic = true;
    gc.options = "--delete-older-than 7d";
    package = pkgs.nixUnstable;
    settings = {
      experimental-features = ["nix-command" "flakes"];
    };
  };

  boot = {
    extraModulePackages = with config.boot.kernelPackages; [usbip];
    initrd.compressor = "zstd";
    initrd.kernelModules = ["amdgpu"];
    binfmt.emulatedSystems = ["wasm32-wasi" "x86_64-windows"];
    kernelPackages = pkgs.linuxPackages_latest;
    kernelParams = ["psmouse.synaptics_intertouch=0" "mem_sleep_default=deep"];
    loader.efi.canTouchEfiVariables = true;
    loader.systemd-boot = {
      editor = false;
      enable = true;
      memtest86.enable = true;
    };
    readOnlyNixStore = true;
    supportedFilesystems = ["btrfs"];
    tmp.useTmpfs = true;
  };

  security = {
    rtkit.enable = true;
    allowSimultaneousMultithreading = true;
    sudo.enable = true;
    doas.enable = true;
    doas.extraRules = [
      {
        users = ["akill"];
        keepEnv = true;
        persist = true;
      }
    ];
  };

  powerManagement = {
    enable = true;
  };

  networking = {
    firewall = {
      enable = true;
      allowedTCPPorts = [80 443];
    };

    hostName = "nixy";
    nameservers = ["127.0.0.1" "::1"];
    dhcpcd.extraConfig = "nohook resolv.conf";

    extraHosts = ''
      192.168.88.171 jellyfin.mediabox.lan
      192.168.88.171 jellyseerr.mediabox.lan
      192.168.88.171 mediabox.lan
      192.168.88.171 qbittorrent.mediabox.lan
      192.168.88.1   router.lan
      192.168.88.231 workstation.lan
    '';

    networkmanager = {
      enable = true;
      dns = "none";
      wifi.backend = "iwd";
    };

    wireless.iwd = {
      enable = true;
      settings = {
        General = {
          AddressRandomization = "network";
          #EnableNetworkConfiguration = true;
        };
      };
    };
  };

  time.timeZone = "Europe/Sarajevo";

  nixpkgs.config.allowUnfree = true;
  nixpkgs.overlays = [nix-xilinx.overlay];
  environment = {
    homeBinInPath = true;
    variables = {
      PATH = "$HOME/.cargo/bin";
    };
  };

  programs = {
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    zsh.enable = true;
    firejail.enable = true;
    adb.enable = true;
    wireshark.enable = true;
    sway.enable = true;
  };

  # List services that you want to enable:
  systemd = {
    services = {
      "zremap" = {
        description = "Intercepts keyboard udev events";
        wants = ["systemd-udevd.service"];
        wantedBy = ["multi-user.target"];
        serviceConfig.Nice = -20;
        script = ''
          sleep 1
          ${zremap.defaultPackage.x86_64-linux}/bin/zremap \
          /dev/input/by-path/platform-i8042-serio-0-event-kbd
        '';
      };
    };

    extraConfig = ''
      DefaultTimeoutStartSec=30s
      DefaultTimeoutStopSec=30s
    '';
  };

  services = {
    acpid.enable = true;
    btrfs.autoScrub.enable = true;
    dbus.enable = true;
    fstrim.enable = true;
    fwupd.enable = true;
    ntp.enable = true;
    openssh.enable = true;
    printing.enable = true;

    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };

    xserver = {
      enable = true;
      dpi = 144;
      libinput.enable = true;
      desktopManager.xterm.enable = false;
      displayManager = {
        lightdm.enable = false;
        startx.enable = true;
        defaultSession = "none+i3";
      };
      windowManager.i3.enable = true;
    };

    udev = {
      packages = [pkgs.rtl-sdr pkgs.openhantek6022];
      extraRules = ''
        #Xilinx FTDI
        ACTION=="add", ATTR{idVendor}=="0403", ATTR{manufacturer}=="Xilinx", MODE:="666"

        #Xilinx Digilent
        ATTR{idVendor}=="1443", MODE:="666"
        ACTION=="add", ATTR{idVendor}=="0403", ATTR{manufacturer}=="Digilent", MODE:="666"
      '';
    };

    tlp = {
      enable = true;
    };

    actkbd = {
      enable = true;
      bindings = [
        {
          keys = [113];
          events = ["key"];
          command = "/run/current-system/sw/bin/runuser -l akill -c 'amixer -q set Master toggle'";
        }

        {
          keys = [114];
          events = ["key" "rep"];
          command = "/run/current-system/sw/bin/runuser -l akill -c 'amixer -q set Master 5%- unmute'";
        }

        {
          keys = [115];
          events = ["key" "rep"];
          command = "/run/current-system/sw/bin/runuser -l akill -c 'amixer -q set Master 5%+ unmute'";
        }

        {
          keys = [224];
          events = ["key"];
          command = "${pkgs.light}/bin/light -U 5";
        }

        {
          keys = [225];
          events = ["key"];
          command = "${pkgs.light}/bin/light -A 5";
        }
      ];
    };

    dnscrypt-proxy2 = {
      enable = true;
      settings = {
        ipv6_servers = true;
        require_dnssec = true;

        sources.public-resolvers = {
          urls = [
            "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md"
            "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
          ];
          cache_file = "/var/lib/dnscrypt-proxy2/public-resolvers.md";
          minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
        };
      };
    };

    nix-serve = {
      enable = false;
      secretKeyFile = "/var/cache-priv-key.pem";
    };

    journald.extraConfig = ''
      SystemMaxUse=50M
    '';

    logind.extraConfig = ''
      KillUserProcesses=yes
    '';
  };

  fonts = {
    fontconfig = {
      cache32Bit = true;
      allowBitmaps = true;
      useEmbeddedBitmaps = true;
      defaultFonts = {
        monospace = ["JetBrainsMono"];
      };
    };

    packages = with pkgs; [
      dejavu_fonts
      dina-font
      fira-code
      fira-code-symbols
      font-awesome
      font-awesome_4
      inconsolata
      iosevka
      jetbrains-mono
      liberation_ttf
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      proggyfonts
      siji
      terminus_font
      terminus_font_ttf
      ubuntu_font_family
    ];
  };

  virtualisation = {
    containers.storage.settings = {
      storage = {
        driver = "btrfs";
        graphroot = "/var/lib/containers/storage";
        runroot = "/run/containers/storage";
      };
    };
    podman = {
      enable = true;
      autoPrune.enable = true;
      dockerCompat = true;
    };
  };

  sound.enable = true;

  hardware = {
    bluetooth = {
      enable = true;
      settings = {
        General = {
          Enable = "Source,Sink,Media,Socket";
        };
      };
    };

    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
      extraPackages = with pkgs; [];
    };
  };

  zramSwap = {
    enable = false;
    algorithm = "zstd";
  };

  users.users.akill = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = ["wireshark" "kvm" "tty" "audio" "sound" "adbusers" "dialout" "wheel"];
  };
}
