{
  config,
  pkgs,
  lib,
  ...
}: {
  imports = [
  ];

  system.stateVersion = "23.05";
  system.autoUpgrade.enable = true;

  nix = {
    optimise.automatic = true;
    gc.automatic = true;
    gc.options = "--delete-older-than 7d";
    package = pkgs.nixUnstable;
    settings.experimental-features = ["nix-command" "flakes"];
  };

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    kernelParams = ["msr.allow_writes=on"];
    supportedFilesystems = ["btrfs"];
    tmpOnTmpfs = true;
    initrd.compressor = "zstd";
    loader.systemd-boot = {
      enable = true;
      editor = false;
      memtest86.enable = true;
    };
    loader.efi.canTouchEfiVariables = true;
    readOnlyNixStore = true;
  };

  security = {
    rtkit.enable = true;
    allowSimultaneousMultithreading = true;
    sudo.enable = false;
    doas.enable = true;
    doas.extraRules = [
      {
        users = ["akill"];
        keepEnv = true;
        persist = true;
      }
    ];
  };

  powerManagement = {
    enable = true;
  };

  networking = {
    firewall.enable = true;
    hostName = "blue";
    nameservers = ["127.0.0.1" "::1"];
    dhcpcd.extraConfig = "nohook resolv.conf";

    networkmanager = {
      enable = true;
      dns = "none";
      /*
      wifi.backend = "iwd";
      */
    };
  };

  time.timeZone = "Europe/Sarajevo";

  nixpkgs.config.allowUnfree = true;
  environment = {
    homeBinInPath = true;
    variables = {
      PATH = "$HOME/.cargo/bin";
    };
  };

  programs = {
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    zsh.enable = true;
    firejail.enable = true;
    adb.enable = true;
    wireshark.enable = true;
    sway.enable = true;
  };

  # List services that you want to enable:
  systemd = {
    services = {
      "caps2esc" = {
        description = "Intercepts keyboard udev events";
        wants = ["systemd-udevd.service"];
        wantedBy = ["multi-user.target"];
        serviceConfig.Nice = -20;
        script = ''
          ${pkgs.interception-tools}/bin/intercept \
          -g /dev/input/by-path/*-kbd | \
          /opt/caps2esc  | ${pkgs.interception-tools}/bin/uinput   \
          -d /dev/input/by-path/*-kbd   \
        '';
      };
    };

    extraConfig = ''
      DefaultTimeoutStartSec=30s
      DefaultTimeoutStopSec=30s
    '';
  };

  services = {
    acpid.enable = true;
    btrfs.autoScrub.enable = true;
    dbus.enable = true;
    fstrim.enable = true;
    fwupd.enable = true;
    ntp.enable = true;
    openssh.enable = false;
    printing.enable = true;

    nextcloud = {
      enable = true;
      hostName = "localhost";
      config.adminpassFile = "${pkgs.writeText "adminpass" "test123"}";
    };

    jellyfin = {
      enable = false;
      user = "akill";
      openFirewall = false;
    };

    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };

    xserver = {
      enable = true;
      dpi = 144;
      libinput.enable = true;
      desktopManager.xterm.enable = false;
      displayManager = {
        lightdm.enable = false;
        startx.enable = true;
        defaultSession = "none+i3";
      };
      windowManager.i3.enable = true;
    };

    udev.packages = [pkgs.rtl-sdr pkgs.openhantek6022];

    tlp = {
      enable = true;
    };

    actkbd = {
      enable = true;
      bindings = [
        {
          keys = [113];
          events = ["key"];
          command = "/run/current-system/sw/bin/runuser -l akill -c 'amixer -q set Master toggle'";
        }

        {
          keys = [114];
          events = ["key" "rep"];
          command = "/run/current-system/sw/bin/runuser -l akill -c 'amixer -q set Master 5%- unmute'";
        }

        {
          keys = [115];
          events = ["key" "rep"];
          command = "/run/current-system/sw/bin/runuser -l akill -c 'amixer -q set Master 5%+ unmute'";
        }

        {
          keys = [224];
          events = ["key"];
          command = "${pkgs.light}/bin/light -U 5";
        }

        {
          keys = [225];
          events = ["key"];
          command = "${pkgs.light}/bin/light -A 5";
        }
      ];
    };

    mpd = {
      musicDirectory = "/home/mpd/music";
      enable = false;
      extraConfig = ''
        audio_output {
          type "pulse"
          name "pulsee srv"
          server "127.0.0.1"
        }
      '';
    };

    dnscrypt-proxy2 = {
      enable = true;
      settings = {
        ipv6_servers = true;
        require_dnssec = true;

        sources.public-resolvers = {
          urls = [
            "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md"
            "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
          ];
          cache_file = "/var/lib/dnscrypt-proxy2/public-resolvers.md";
          minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
        };
      };
    };

    journald.extraConfig = ''
      SystemMaxUse=50M
    '';

    logind.extraConfig = ''
      KillUserProcesses=yes
    '';
  };

  fonts = {
    fontconfig = {
      cache32Bit = true;
      allowBitmaps = true;
      useEmbeddedBitmaps = true;
      defaultFonts = {
        monospace = ["JetBrainsMono"];
      };
    };

    fonts = with pkgs; [
      dejavu_fonts
      dina-font
      fira-code
      fira-code-symbols
      font-awesome
      font-awesome_4
      inconsolata
      iosevka
      jetbrains-mono
      liberation_ttf
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      proggyfonts
      siji
      terminus_font
      terminus_font_ttf
      ubuntu_font_family
    ];
  };

  virtualisation = {
    containers.storage.settings = {
      storage = {
        driver = "btrfs";
        graphroot = "/var/lib/containers/storage";
        runroot = "/run/containers/storage";
      };
    };
    podman = {
      enable = true;
      dockerCompat = true;
    };
  };

  sound.enable = true;

  hardware = {
    bluetooth = {
      enable = true;
      settings = {
        General = {
          Enable = "Source,Sink,Media,Socket";
        };
      };
    };

    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
      extraPackages = with pkgs; [
      ];
    };
  };

  zramSwap = {
    enable = false;
    algorithm = "zstd";
  };

  users.users.akill = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = ["wireshark" "kvm" "tty" "audio" "sound" "adbusers" "dialout"];
  };
}
