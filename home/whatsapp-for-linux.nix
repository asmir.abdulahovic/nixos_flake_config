{
  config,
  pkgs,
  lib,
  ...
}:
with lib; {
  xdg.configFile."whatsapp-for-linux/settings.conf".source = builtins.toFile "settings.conf" (
    generators.toINI {} {
      General = {
        zoom_level = 1;
        close_to_tray = false;
      };

      Network = {
        allow_permissions = true;
      };

      web = {
        allow-permissions = true;
        hw-accel = 1;
      };

      general = {
        notification-sounds = true;
        close-to-tray = true;
        start-in-tray = false;
      };

      appearance = {
        prefer-dark-theme = true;
      };
    }
  );
}
