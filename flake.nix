{
  description = "NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    nix-xilinx = {
      url = "gitlab:asmir.abdulahovic/nix-xilinx";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    peerix = {
      url = "gitlab:asmir.abdulahovic/peerix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    zremap = {
      url = "gitlab:asmir.abdulahovic/zremap";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    swaysw = {
      url = "git+https://git.project-cloud.net/asmir/swaysw";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nvim = {
      url = "git+https://git.project-cloud.net/asmir/nvim_flake";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    simple-nixos-mailserver = {
      url = "gitlab:simple-nixos-mailserver/nixos-mailserver";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {
    home-manager,
    nixpkgs,
    nix-xilinx,
    nvim,
    peerix,
    simple-nixos-mailserver,
    sops-nix,
    swaysw,
    zremap,
    ...
  }: let
    pkgs = nixpkgs.legacyPackages.x86_64-linux.pkgs;
  in {
    nixosConfigurations = rec {
      nixy = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          {_module.args = inputs;}
          ./nixy/configuration.nix
          ./nixy/hardware-configuration.nix
          ./common/packages.nix
          sops-nix.nixosModules.sops
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.akill = import ./home/home.nix;
            home-manager.extraSpecialArgs = {inherit inputs;};
          }
          peerix.nixosModules.peerix
          {
            services.peerix = {
              enable = true;
              globalCacheTTL = 10;
              package = peerix.packages.x86_64-linux.peerix;
              openFirewall = true; # UDP/12304
              privateKeyFile = nixy.config.sops.secrets."peerix/private".path;
              publicKeyFile = ./nixy/peerix-public;
              publicKey = "peerix-mediabox:UDgG3xdQYv7bmx2l4ZPNRPJtp2zMmY++H/fnGeJ9BQw=";
            };
          }
        ];
      };

      mediabox = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          {_module.args = inputs;}
          ./common/packages.nix
          ./common/suspend.nix
          ./mediabox/configuration.nix
          ./mediabox/hardware-configuration.nix
          ./modules/qbittorrent.nix
          sops-nix.nixosModules.sops
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.akill = import ./home/home.nix;
            home-manager.extraSpecialArgs = {inherit inputs;};
          }
          peerix.nixosModules.peerix
          {
            services.peerix = {
              enable = true;
              globalCacheTTL = 10;
              package = peerix.packages.x86_64-linux.peerix;
              openFirewall = true; # UDP/12304
              privateKeyFile = mediabox.config.sops.secrets."peerix/private".path;
              publicKeyFile = ./mediabox/peerix-public;
              publicKey = "peerix-nixy:8THqS0R2zWF/47ai0RFmqJnieYTZ1jaWOD9tnzpvA6s=";
            };
          }
        ];
      };

      blue = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          {_module.args = inputs;}
          ./blue/configuration.nix
          ./blue/hardware-configuration.nix
          ./common/packages.nix
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.akill = import ./home/home.nix;
          }
        ];
      };
      magpie = nixpkgs.lib.nixosSystem {
        system = "arm64-linux";
        modules = [
          {_module.args = inputs;}
          ./magpie/configuration.nix
          ./magpie/hardware-configuration.nix
          sops-nix.nixosModules.sops
          simple-nixos-mailserver.nixosModule
          (builtins.toPath "${nixpkgs}/nixos/modules/profiles/qemu-guest.nix")
        ];
      };
    };

    devShell.x86_64-linux = pkgs.mkShell {
      buildInputs = with pkgs; [sops ssh-to-age age];
      shellHook = ''
        echo "Configuring NixOS!"
      '';
    };

    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.alejandra;
  };
}
