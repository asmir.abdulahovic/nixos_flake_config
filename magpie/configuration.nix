{
  config,
  pkgs,
  lib,
  ...
}: {
  imports = [];

  nix.optimise.automatic = true;
  nix.settings.experimental-features = ["nix-command" "flakes"];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.grub.devices = ["/dev/sda"];
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  users.users.root.initialHashedPassword = "";
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC0gyN7DzF7+sinneq7++fT93dNWe9ttKnLZJEb0LVs7UxPtz/ovlxnktAgEtSh7NUUGKPILGG6+YG/Jz3pb4cLuQHtavIQ2mIzIbiNl+c80gLNPulfOrC3KyCacYnlcEpoV+4yvMPLDf+5ySilYoF30CSIo8B7B4PSwO3/I20oXXY0zeVmYKs65BY8OrR8PDdtPpuqGcTdPpVSrooZQoykriFeejBb0Jn7qWO7vmsTyUZZIP4nKKUyqE6iFZ2zv+J3mYfuoglQKO1+kqcCYCef0sheLZGD4/QIIL8HJ9yNWb6OQhu7MEv1NowuHkviImwVO3actZ1/x4lrWt4mY+bGglVwA90u1KZUQ10qKQ2xCG2ZHE9DSxWxpI/Yq2P4pLA/XSkYFPpzmoD9c6cpv0WLAvmQrEVkqK0xXo+KszUlyGy5sVJl7/h1fZ8YhWsWUnU1XJFmKLaomUZflL3h7X6xJNVPzZmso8l1INdCvIBDu+G84kAp1/aFalSJMyjTgvCc1hxhAVYhmrc3msGH0Jk8CcPBwYa0BH4EryacdupOS/c5VxAbdyuizEgitP1ylRmydVVDEItPNXFvpWdyEehf/VmsUXqL48mBzfvi6feD5AzKjPaQNaATpxLs9Sl9CMxSy27ahHwEK6dek1wm7nkoSIDSRWfGhYKr3lUg0emAYQ=="
  ];

  environment.systemPackages = with pkgs; [
    alejandra
    curl
    fd
    file
    fzf
    fzy
    git
    htop
    tig
    unzip
    wget
    zip
  ];

  programs.mosh.enable = true;
  programs.neovim = {
    enable = true;
    vimAlias = true;
    viAlias = true;
  };

  mailserver = {
    enable = true;
    debug = false;
    fqdn = "mail.project-cloud.net";
    domains = ["project-cloud.net"];
    enableSubmissionSsl = true;
    enableImap = false;
    enableImapSsl = true;

    # A list of all login accounts. To create the password hashes, use
    # nix-shell -p mkpasswd --run 'mkpasswd -sm bcrypt'
    loginAccounts = {
      "gitea@project-cloud.net" = {
        hashedPasswordFile = config.sops.secrets."gitea_mail_pw_hash".path;
        aliases = ["git@project-cloud.net"];
      };
      "asmir@project-cloud.net" = {
        hashedPasswordFile = config.sops.secrets."asmir_mail_pw_hash".path;
        aliases = ["asmir.abdulahovic@project-cloud.net"];
      };
    };
    certificateScheme = "acme-nginx";
  };

  services.journald.extraConfig = ''SystemMaxUse=50M '';
  services.logind.extraConfig = ''KillUserProcesses=yes '';
  services.openssh.settings.PermitRootLogin = "prohibit-password";
  services.openssh.enable = true;
  services.opendkim.enable = true;

  services.miniflux = {
    enable = true;
    adminCredentialsFile = config.sops.secrets."miniflux_env".path;
    config = {
      LISTEN_ADDR = "localhost:5001";
      BASE_URL = "https://miniflux.project-cloud.net";
    };
  };

  services.restya-board = {
    enable = true;
    /*
    virtualHost.serverName = "board.project-cloud.net";
    */
    virtualHost.listenHost = "localhost";
    virtualHost.listenPort = 4001;
  };

  services.nextcloud = rec {
    enable = true;
    package = pkgs.nextcloud27;
    config.adminpassFile = config.sops.secrets."nextcloud_admin".path;
    configureRedis = true;
    hostName = "project-cloud.net";
    https = true;
    extraOptions = {
      mail_smtpmode = "sendmail";
      mail_sendmailmode = "pipe";
      enabledPreviewProviders = [
        "OC\\Preview\\BMP"
        "OC\\Preview\\GIF"
        "OC\\Preview\\HEIC"
        "OC\\Preview\\JPEG"
        "OC\\Preview\\Krita"
        "OC\\Preview\\MarkDown"
        "OC\\Preview\\MP3"
        "OC\\Preview\\OpenDocument"
        "OC\\Preview\\PNG"
        "OC\\Preview\\TXT"
        "OC\\Preview\\XBitmap"
      ];
    };
    phpOptions =
      phpOptions.default
      // {
        "opcache.jit" = "tracing";
        "opcache.jit_buffer_size" = "100M";
        # recommended by nextcloud admin overview
        "opcache.interned_strings_buffer" = "16";
      };
  };

  services.nginx = {
    enable = true;
    package = pkgs.nginxQuic;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    virtualHosts.${config.services.nextcloud.hostName} = {
      quic = true;
      forceSSL = true;
      enableACME = true;
    };

    virtualHosts."miniflux.project-cloud.net" = {
      quic = true;
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://localhost:5001";
      };
    };

    virtualHosts.${config.services.gitea.settings.server.DOMAIN} = {
      quic = true;
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://localhost:${toString config.services.gitea.settings.server.HTTP_PORT}";
      };
    };

    virtualHosts."board.project-cloud.net" = {
      quic = true;
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://localhost:${toString config.services.restya-board.virtualHost.listenPort}";
      };
    };
  };

  services.gitea = {
    enable = true;
    appName = "Project Cloud Gitea server";
    database = {
      type = "sqlite3";
      passwordFile = config.sops.secrets."gitea_db".path;
    };
    settings.server = {
      DOMAIN = "git.project-cloud.net";
      ROOT_URL = "https://git.project-cloud.net";
      DISABLE_SSH = true;
      HTTP_PORT = 3001;
    };
    settings.mailer = {
      ENABLED = true;
      FROM = "gitea@project-cloud.net";
      PROTOCOL = "sendmail";
      SENDMAIL_PATH = "/run/wrappers/bin/sendmail";
    };

    settings.service = {
      REGISTER_EMAIL_CONFIRM = true;
      DISABLE_REGISTRATION = true;
    };

    settings."markup.restructuredtext" = let
      docutils =
        pkgs.python3.withPackages (ps: with ps; [docutils pygments]);
    in {
      ENABLED = true;
      FILE_EXTENSIONS = ".rst";
      RENDER_COMMAND = "${docutils}/bin/rst2html.py";
      IS_INPUT_FILE = false;
    };
  };

  /*
  needed for sendmail mail functionality
  */
  users.users.gitea.extraGroups = ["postdrop"];
  systemd.services.gitea.serviceConfig = {
    RestrictAddressFamilies = ["AF_NETLINK"];
    ProtectSystem = lib.mkForce false;
  };

  security.acme = {
    acceptTerms = true;
    defaults.email = "asmir.abdulahovic@gmail.com";
  };

  sops.age.sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key"];

  sops.secrets."miniflux_env" = {
    sopsFile = ./secrets/miniflux.yaml;
  };

  sops.secrets."gitea_mail_pw_hash" = {
    sopsFile = ./secrets/gitea_mail_pw_hash.yaml;
  };

  sops.secrets."asmir_mail_pw_hash" = {
    sopsFile = ./secrets/asmir_mail_pw_hash.yaml;
  };

  sops.secrets."gitea_db" = {
    sopsFile = ./secrets/gitea_db.yaml;
    owner = config.users.users.gitea.name;
  };

  sops.secrets."nextcloud_admin" = {
    sopsFile = ./secrets/nextcloud_admin.yaml;
    owner = config.users.users.nextcloud.name;
  };

  networking.hostName = "magpie";
  networking.wireless.enable = false;
  networking.firewall.enable = true;
  networking.firewall.allowedTCPPorts = [80 443 587];
  networking.firewall.allowedUDPPorts = [];
  networking.networkmanager.enable = true;

  system.stateVersion = "22.11";
}
